﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MarkovTextGenenerator
{
    public partial class Form1 : Form
    {
        private int order;
        private SortedDictionary<string, List<string>> ngramDict;

        private int lenght;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click( object sender, EventArgs e )
        {
            ngramDict = new SortedDictionary<string, List<string>>();
            string inputWithExtraSpaces = this.inputBox.Text.Replace( Environment.NewLine, " " );
            string input = System.Text.RegularExpressions.Regex.Replace( inputWithExtraSpaces, @"\s+", " " );
            order = (int)this.orderUpDown.Value;
            lenght = (int)this.lenghtUpDown.Value;

            populateNGramList( input );

            string output = generateText( input );
            output = capitalizeStartOfSentence( output );
            output = capitalizeSingleI( output );

            if( this.periodBox.Checked )
            {
                output = AddPeriodAtTheEnd( output );
            }

            this.nGramCount.Text = ngramDict.Count.ToString();
            this.outputBox.Text = output;
        }

        private void populateNGramList( string input )
        {
            for( int i = 0; i <= input.Length - order; i++ )
            {
                string ngram = input.Substring( i, order ).ToLower();
                if( !ngramDict.ContainsKey( ngram ) )
                {
                    ngramDict.Add( ngram, new List<string>() );
                }

                string letter = " ";
                if( input.Length > i + order )
                {
                    letter = input.Substring( i + order, 1 ).ToLower();
                }
                ngramDict[ngram].Add( letter );
            }
        }

        private string generateText( string input )
        {
            Random random = new Random();
            string start = input.Substring( 0, order ).ToLower();
            string regex = @"\s[a-z]{" + ( order - 1 ) + @",}";

            do
            {
                start = ngramDict.Keys.ElementAt( random.Next( ngramDict.Count ) );
            }
            while( !Regex.IsMatch( start, regex ) );

            string currentGram = start;

            for( int i = 0; i < lenght; i++ )
            {
                List<string> possibilities;
                if( ngramDict.TryGetValue( currentGram, out possibilities ) )
                {
                    start += possibilities[random.Next( possibilities.Count )];
                    currentGram = start.Substring( start.Length - order, order ).ToLower();
                }
            }
            return start.Trim();
        }

        private string capitalizeStartOfSentence( string text )
        {
            string[] sentences = Regex.Split( text, @"(?<=[\.!\?])\s+" );

            for( int i = 0; i < sentences.Length; i++ )
            {
                sentences[i] = sentences[i].First().ToString().ToUpper() + sentences[i].Substring( 1 );
            }
            return string.Join( " ", sentences );
        }

        private string AddPeriodAtTheEnd( string text )
        {
            int lastPunctation = text.LastIndexOfAny( new char[] { '.', '?', '!' } );
            if( lastPunctation != text.Length - 1 )
            {
                text += ".";
            }
            return text;
        }

        private string capitalizeSingleI( string text )
        {
            return text.Replace( " i ", " I " );
        }
    }
}
